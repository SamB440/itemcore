package net.islandearth.itemcore.spells;

import org.bukkit.entity.Player;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.islandearth.itemcore.items.CustomItem;

@AllArgsConstructor
public abstract class SpellAction {
	
	@Getter
	@Setter
	private boolean shift;
	
	abstract void use(Player player, CustomItem item);
	
}
