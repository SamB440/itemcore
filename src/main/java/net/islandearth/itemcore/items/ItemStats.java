package net.islandearth.itemcore.items;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class ItemStats {
	
	@Getter
	@Setter
	private int reloadSpeed;
	
	@Getter
	@Setter
	private int projectileSpeed;
	
	@Getter
	@Setter
	private int power;
	
	@Getter
	@Setter
	private int accuracy;
	
	@Getter
	@Setter
	private int additionalRange;
	
	@Getter
	@Setter
	private int manaUse;
}
