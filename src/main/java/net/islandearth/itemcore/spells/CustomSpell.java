package net.islandearth.itemcore.spells;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.bukkit.Material;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.islandearth.itemcore.ItemCore;
import net.islandearth.itemcore.serializer.SpellActionSerializer;

@AllArgsConstructor
public class CustomSpell {
	
	@Getter
	@Setter
	private String name;
	
	@Getter
	@Setter
	private String displayName;

	@Getter
	@Setter
	private int range;
	
	@Getter
	@Setter
	private Material material;
	
	@Getter
	@Setter
	private List<String> lore;
	
	@Getter
	@Setter
	private List<SpellParticle> particles;
	
	@Getter
	@Setter
	private List<SpellAction> actions;
	
	public void saveSpell() {
		Gson gson = new GsonBuilder().
				setPrettyPrinting().
				serializeNulls().
				enableComplexMapKeySerialization().
				registerTypeAdapter(SpellAction.class, new SpellActionSerializer()).
				create();
		try {
			Writer writer = new FileWriter(ItemCore.plugin.getDataFolder() + "/spells/" + name + ".json");
			gson.toJson(this, writer);
			writer.close();
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
