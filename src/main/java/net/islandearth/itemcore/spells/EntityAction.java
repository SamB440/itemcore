package net.islandearth.itemcore.spells;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;
import net.islandearth.itemcore.ItemCore;
import net.islandearth.itemcore.items.CustomItem;

public class EntityAction extends SpellAction {

	@Getter
	@Setter
	private SpawnLocation spawnLocation;
	
	@Getter
	@Setter
	private EntityType entity;
	
	public EntityAction(boolean shift, SpawnLocation spawnLocation, EntityType entity) {
		super(shift);
		
		this.spawnLocation = spawnLocation;
		this.entity = entity;
	}

	@Override
	public void use(Player player, CustomItem item) {
		CustomSpell spell = ItemCore.plugin.getCache().getSpellFor(player, item);
		if (spell != null) {
			
			Location location1 = player.getTargetBlock(null, spell.getRange() + item.getStats().getAdditionalRange()).getLocation();
			Location location2 = player.getEyeLocation();
			
			switch (spawnLocation) {
				case EYE:
					player.getWorld().spawnEntity(location2, entity);
					break;
					
				case TARGET:
					player.getWorld().spawnEntity(location1, entity);
					break;
					
				default:
					break;
			}
		}
	}
}
