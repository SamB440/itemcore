package net.islandearth.itemcore.items;

import lombok.Getter;

public enum WandTier implements ItemTier {
	STARTING(TierOption.FIRST_JOIN),
	BROKEN,
	OLD,
	QUICK,
	EFFICIENT,
	BALANCED,
	POLISHED,
	SHINING,
	LEGENDARY,
	ANCIENT,
	DEMONIC,
	CREATOR;
	
	@Getter
	private TierOption[] tierOptions;
	
	private WandTier(TierOption... tierOptions) {
		this.tierOptions = tierOptions;
	}

	@Override
	public ItemTier getTier() {
		return this;
	}
}
