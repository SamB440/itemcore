package net.islandearth.itemcore.provider;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import lombok.AllArgsConstructor;
import net.islandearth.itemcore.ItemCore;

@AllArgsConstructor
public class FallListener implements Listener {
	
	private ItemCore plugin;
	
	@EventHandler
	public void onFall(EntityDamageEvent ede) {
		if (ede.getEntity() instanceof Player) {
			Player player = (Player) ede.getEntity();
			if (ede.getCause() == DamageCause.FALL) {
				if (plugin.getActions().containsKey(player.getUniqueId())) {
					ede.setCancelled(true);
				}
			}
		}
	}
}
