package net.islandearth.itemcore.api;

import org.bukkit.entity.Player;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class ActionPlayer {

	@Getter
	@Setter
	private Player player;
	
	@Getter
	@Setter
	private double blocks;
}
