package net.islandearth.itemcore.provider;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import lombok.AllArgsConstructor;
import net.islandearth.itemcore.ItemCore;
import net.islandearth.itemcore.items.CustomItem;
import net.islandearth.itemcore.items.TierOption;

@AllArgsConstructor
public class JoinListener implements Listener {
	
	private ItemCore plugin;
	
	@EventHandler
	public void onJoin(PlayerJoinEvent pje) {
		Player player = pje.getPlayer();
		if (!player.hasPlayedBefore()) {
			List<CustomItem> items = plugin.getItemRegistry().getItemsWithTier(TierOption.FIRST_JOIN);
			for (CustomItem item : items) {
				player.getInventory().addItem(item.toItemStack());
			}
		}
	}
}
