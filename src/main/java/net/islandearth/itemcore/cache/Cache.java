package net.islandearth.itemcore.cache;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;

import net.islandearth.itemcore.items.CustomItem;
import net.islandearth.itemcore.spells.CustomSpell;

public class Cache {
	
	private Map<Player, Map<CustomItem, CustomSpell>> selectedSpells = new HashMap<>();
	
	public CustomSpell getSpellFor(Player player, CustomItem item) {
		if (selectedSpells.containsKey(player)) {
			if (selectedSpells.get(player).containsKey(item)) {
				return selectedSpells.get(player).get(item);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public void setSpellFor(Player player, CustomItem item, CustomSpell spell) {
		if (selectedSpells.containsKey(player)) {
			Map<CustomItem, CustomSpell> custom = selectedSpells.get(player);
			custom.put(item, spell);
			selectedSpells.put(player, custom);
		} else {
			Map<CustomItem, CustomSpell> custom = new HashMap<>();
			custom.put(item, spell);
			selectedSpells.put(player, custom);
		}
	}
}
