package net.islandearth.itemcore.spells;

import org.bukkit.Particle;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class SpellParticle {
	
	@Getter
	@Setter
	private Particle particle;
	
	@Getter
	@Setter
	private int count;
	
	@Getter
	@Setter
	private double speed;
	
	@Getter
	@Setter
	private double offSetX;
	
	@Getter
	@Setter
	private double offSetY;
	
	@Getter
	@Setter
	private double offSetZ;
	
	@Getter
	@Setter
	private ParticleType type;
	
	@Getter
	@Setter
	private int red;
	
	@Getter
	@Setter
	private int green;
	
	@Getter
	@Setter
	private int blue;
	
	public enum ParticleType {
		DEFAULT,
		COLOUR
	}
}
