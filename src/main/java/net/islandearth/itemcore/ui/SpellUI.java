package net.islandearth.itemcore.ui;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.islandearth.itemcore.ItemCore;
import net.islandearth.itemcore.items.CustomItem;
import net.islandearth.itemcore.spells.CustomSpell;

public class SpellUI extends UI {

	public SpellUI(int size, String name, CustomItem customItem) {
		super(size, name);
		List<String> spells = customItem.getSpells();
		for (int i = 0; i < spells.size(); i++) {
			CustomSpell spell = ItemCore.plugin.getSpellRegistry().getSpell(spells.get(i));
			ItemStack item = new ItemStack(spell.getMaterial());
			ItemMeta im = item.getItemMeta();
			im.setLore(spell.getLore());
			im.setDisplayName(spell.getDisplayName());
			item.setItemMeta(im);
			setItem(i, item, player -> {
				ItemCore.plugin.getCache().setSpellFor(player, customItem, spell);
				player.sendMessage(ChatColor.GREEN + "Spell set!");
				player.closeInventory();
			});
		}
	}
}
