package net.islandearth.itemcore;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.command.CommandMap;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.Getter;
import net.islandearth.itemcore.api.ActionPlayer;
import net.islandearth.itemcore.cache.Cache;
import net.islandearth.itemcore.commands.GiveItem;
import net.islandearth.itemcore.items.ComplexItem;
import net.islandearth.itemcore.items.CustomItem;
import net.islandearth.itemcore.items.ItemRegistry;
import net.islandearth.itemcore.items.ItemStats;
import net.islandearth.itemcore.items.ItemTier;
import net.islandearth.itemcore.items.WandTier;
import net.islandearth.itemcore.provider.FallListener;
import net.islandearth.itemcore.provider.InventoryListener;
import net.islandearth.itemcore.provider.ItemListener;
import net.islandearth.itemcore.provider.JoinListener;
import net.islandearth.itemcore.serializer.InterfaceSerializer;
import net.islandearth.itemcore.serializer.SpellActionSerializer;
import net.islandearth.itemcore.spells.CustomSpell;
import net.islandearth.itemcore.spells.EntityAction;
import net.islandearth.itemcore.spells.SpawnLocation;
import net.islandearth.itemcore.spells.SpellAction;
import net.islandearth.itemcore.spells.SpellParticle;
import net.islandearth.itemcore.spells.SpellParticle.ParticleType;
import net.islandearth.itemcore.spells.SpellRegistry;
import net.islandearth.itemcore.spells.VelocityAction;

public class ItemCore extends JavaPlugin {
	
	public static ItemCore plugin;
	@Getter private ItemRegistry itemRegistry;
	@Getter private SpellRegistry spellRegistry;
	@Getter private Cache cache;
	@Getter private List<UUID> onCooldown = new ArrayList<>();
	@Getter private Map<UUID, ActionPlayer> actions = new HashMap<>();
	
	@Override
	public void onEnable() {
		createFiles();
		ItemCore.plugin = this;
		this.spellRegistry = new SpellRegistry();
		this.itemRegistry = new ItemRegistry();
		this.cache = new Cache();
		loadSpells();
		loadItems();
		registerProviders();
		registerCommands();
	}
	
	@Override
	public void onDisable() {
		ItemCore.plugin = null;
	}
	
	private void createFiles() {
		File dir1 = new File(this.getDataFolder() + "/items/builtin/");
		if (!dir1.exists()) dir1.mkdirs();
		
		File dir2 = new File(this.getDataFolder() + "/items/complex/");
		if (!dir2.exists()) dir2.mkdirs();
		
		File dir3 = new File(this.getDataFolder() + "/spells/");
		if (!dir3.exists()) dir3.mkdirs();
	}
	
	private void loadSpells() {
		File spell1 = new File(this.getDataFolder() + "/spells/default.json");
		if (!spell1.exists()) {
			CustomSpell spell = new CustomSpell("default", 
					ChatColor.BLUE + "Default", 
					4, 
					Material.STICK, 
					Arrays.asList(ChatColor.WHITE + "" + ChatColor.ITALIC + "Default spell"), 
					Arrays.asList(new SpellParticle(Particle.CLOUD, 1, 0.01, 0.2, 0.01, 0.001, ParticleType.DEFAULT, 0, 0, 0), 
							new SpellParticle(Particle.CRIT, 1, 0.01, 0.2, 0.01, 0.001, ParticleType.DEFAULT, 0, 0, 0), 
							new SpellParticle(Particle.REDSTONE, 1, 0.01, 0.2, 0.01, 0.001, ParticleType.COLOUR, 0, 255, 239)),
					Arrays.asList(new VelocityAction(false, 0, 1, 0),
							new EntityAction(true, SpawnLocation.EYE, EntityType.FIREBALL)));
			spell.saveSpell();
		}
		
		File dir1 = new File(this.getDataFolder() + "/spells/");
		for (File file : dir1.listFiles()) {
			Gson gson = new GsonBuilder().
					setPrettyPrinting().
					serializeNulls().
					enableComplexMapKeySerialization().
					registerTypeAdapter(SpellAction.class, new SpellActionSerializer()).
					create();
			try {
				Reader reader = new FileReader(file);
				CustomSpell spell = gson.fromJson(reader, CustomSpell.class);
				spellRegistry.addSpell(spell);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void loadItems() {
		
		File item1 = new File(this.getDataFolder() + "/items/builtin/test.json");
		if (!item1.exists()) {
			CustomItem newItem = new CustomItem(true, "test", Material.STICK, Arrays.asList("test"), "Test", Arrays.asList(WandTier.STARTING), 1, new ItemStats(60, 1, 1, 1, 1, 1), Arrays.asList("default"));
			newItem.saveItem();
		}
		
		File item2 = new File(this.getDataFolder() + "/items/complex/complex.json");
		if (!item2.exists()) {
			Map<String, String> customTiers = new HashMap<>();
			customTiers.put("TIER", "FIRST_JOIN");
			
			ComplexItem complexItem = new ComplexItem(true, "complex", Material.STICK, Arrays.asList("test"), "Test", Arrays.asList(WandTier.STARTING), 1, new ItemStats(1, 1, 1, 1, 1, 1), customTiers, Arrays.asList("default"));
			complexItem.saveItem();
		}
		
		File dir1 = new File(this.getDataFolder() + "/items/builtin/");
		for (File file : dir1.listFiles()) {
			Gson gson = new GsonBuilder().
					setPrettyPrinting().
					serializeNulls().
					registerTypeAdapter(ItemTier.class, new InterfaceSerializer<WandTier>(WandTier.class)).
					create();
			try {
				Reader reader = new FileReader(file);
				CustomItem item = gson.fromJson(reader, CustomItem.class);
				List<String> lore = item.getLore();
				for (int i = 0; i < lore.size(); i++) {
					double reloadSpeed = item.getStats().getReloadSpeed() / 20.0;
					if (reloadSpeed >= 60.0) {
						reloadSpeed = reloadSpeed / 60.0;
					}
					
					lore.set(i, lore.get(i).replace("%reloadSpeed%", "" + reloadSpeed));
					lore.set(i, lore.get(i).replace("%projectileSpeed%", "" + item.getStats().getProjectileSpeed()));
					lore.set(i, lore.get(i).replace("%power%", "" + item.getStats().getPower()));
					lore.set(i, lore.get(i).replace("%accuracy%", "" + item.getStats().getAccuracy()));
					lore.set(i, lore.get(i).replace("%additionalRange%", "" + item.getStats().getAdditionalRange()));
					lore.set(i, lore.get(i).replace("%manaUse%", "" + item.getStats().getManaUse()));
				}
				
				if (item.isSoulbound()) {
					lore.add(" ");
					lore.add(ChatColor.GRAY + "Soulbound");
				}
				
				item.setLore(lore);
				itemRegistry.addItem(item);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		File dir2 = new File(this.getDataFolder() + "/items/complex/");
		for (File file : dir2.listFiles()) {
			Gson gson = new GsonBuilder().
					setPrettyPrinting().
					serializeNulls().
					registerTypeAdapter(ItemTier.class, new InterfaceSerializer<WandTier>(WandTier.class)).
					create();
			try {
				Reader reader = new FileReader(file);
				ComplexItem item = gson.fromJson(reader, ComplexItem.class);
				List<String> lore = item.getLore();
				for (int i = 0; i < lore.size(); i++) {
					double reloadSpeed = item.getStats().getReloadSpeed() / 20.0;
					if (reloadSpeed >= 60.0) {
						reloadSpeed = reloadSpeed / 60.0;
					}
					
					lore.set(i, lore.get(i).replace("%reloadSpeed%", "" + reloadSpeed));
					lore.set(i, lore.get(i).replace("%projectileSpeed%", "" + item.getStats().getProjectileSpeed()));
					lore.set(i, lore.get(i).replace("%power%", "" + item.getStats().getPower()));
					lore.set(i, lore.get(i).replace("%accuracy%", "" + item.getStats().getAccuracy()));
					lore.set(i, lore.get(i).replace("%additionalRange%", "" + item.getStats().getAdditionalRange()));
					lore.set(i, lore.get(i).replace("%manaUse%", "" + item.getStats().getManaUse()));
				}
				
				if (item.isSoulbound()) {
					lore.add(" ");
					lore.add(ChatColor.GRAY + "Soulbound");
				}
				
				item.setLore(lore);
				itemRegistry.addItem(item);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void registerProviders() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new JoinListener(this), this);
		pm.registerEvents(new ItemListener(this), this);
		pm.registerEvents(new InventoryListener(), this);
		pm.registerEvents(new FallListener(this), this);
	}
	
	private void registerCommands() {
		try {
			Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
			bukkitCommandMap.setAccessible(true);
			CommandMap commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());

			commandMap.register("GiveItem", new GiveItem());
		} catch (NoSuchFieldException | 
				SecurityException | 
				IllegalArgumentException | 
				IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
