package net.islandearth.itemcore.items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.Getter;

public class ItemRegistry {
	
	@Getter
	private List<CustomItem> registry = new ArrayList<>();
	
	/**
	 * Gets a custom item with its unique name.
	 * @param name - unique name of item
	 * @return a custom item from the given name,
	 * null if the item is not registered.
	 * @since 1.0
	 */
	public CustomItem getItem(String name) {
		for (CustomItem item : registry) {
			if (item.getName().equals(name)) {
				return item;
			}
		}
		return null;
	}
	
	/**
	 * Registers a custom item.
	 * @param item
	 * @since 1.0
	 */
	public void addItem(CustomItem item) {
		registry.add(item);
	}
	
	/**
	 * Removes a custom item.
	 * @param item
	 * @since 1.0
	 */
	public void removeItem(CustomItem item) {
		registry.remove(item);
	}
	
	/**
	 * Gets all items with the provided tier options.
	 * @param options
	 * @return all items with the provided tier options
	 * @since 1.0
	 */
	public List<CustomItem> getItemsWithTier(TierOption... options) {
		List<CustomItem> items = new ArrayList<>();
		for (CustomItem item : registry) {
			for (TierOption option : options) {
				for (ItemTier tiers : item.getTiers()) {
					if (Arrays.asList(tiers.getTierOptions()).contains(option)) {
						if (!items.contains(item)) items.add(item);
						else break;
					}
				}
				
				if (!items.contains(item)) {
					if (item instanceof ComplexItem) {
						ComplexItem complexItem = (ComplexItem) item;
						complexItem.getCustomTiers().forEach((k, v) -> {
							TierOption customOption = TierOption.valueOf(v);
							if (customOption == TierOption.FIRST_JOIN) {
								items.add(item);
							}
						});
					}
				}
			}
		}
		return items;
	}
	
	/**
	 * Gets all items soulbound.
	 * @param options
	 * @return all items soulbound
	 * @since 1.0
	 */
	public List<CustomItem> getSoulboundItems() {
		List<CustomItem> items = new ArrayList<>();
		for (CustomItem item : registry) {
			if (item.isSoulbound()) {
				items.add(item);
			}
		}
		return items;
	}
}
