package net.islandearth.itemcore.spells;

import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import lombok.Getter;
import lombok.Setter;
import net.islandearth.itemcore.items.CustomItem;

public class VelocityAction extends SpellAction {
	
	@Getter
	@Setter
	private double x;
	
	@Getter
	@Setter
	private double y;
	
	@Getter
	@Setter
	private double z;
	
	public VelocityAction(boolean shift, 
			double x, 
			double y, 
			double z) {
		super(shift);
		
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public void use(Player player) {
		player.setVelocity(new Vector(x, y, z));
	}
	
	@Override
	protected void use(Player player, CustomItem item) {
		use(player);
	}
}
