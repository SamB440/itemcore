package net.islandearth.itemcore.items;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;

import lombok.Getter;
import lombok.Setter;
import net.islandearth.itemcore.ItemCore;

public class ComplexItem extends CustomItem implements CustomTier {
	
	@Getter
	@Setter
	private Map<String, String> customTiers;

	public ComplexItem(boolean soulbound, String name, Material material, List<String> lore, String displayName,
			List<ItemTier> tiers, int amount, ItemStats stats, Map<String, String> customTiers, List<String> spells) {
		super(soulbound, name, material, lore, displayName, tiers, amount, stats, spells);
		this.customTiers = customTiers;
	}
	
	@Override
	public void saveItem() {
		Gson gson = new GsonBuilder().
				setPrettyPrinting().
				serializeNulls().
				create();
		try {
			Writer writer = new FileWriter(ItemCore.plugin.getDataFolder() + "/items/complex/" + this.getName() + ".json");
			gson.toJson(this, writer);
			writer.close();
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
