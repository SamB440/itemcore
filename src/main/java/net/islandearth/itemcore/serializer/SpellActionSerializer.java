package net.islandearth.itemcore.serializer;

import java.lang.reflect.Type;

import org.bukkit.Bukkit;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import net.islandearth.itemcore.spells.SpellAction;

public class SpellActionSerializer implements JsonSerializer<SpellAction>, JsonDeserializer<SpellAction> {

	private final String CLASS_META_KEY = "type";

    @Override
    public JsonElement serialize(SpellAction src, 
    		Type typeOfSrc,
    		JsonSerializationContext context) {
        JsonElement element = null;
        if (src == null) {
            return element;
        }
        
        element = context.serialize(src);
        element.getAsJsonObject().addProperty(CLASS_META_KEY, src.getClass().getCanonicalName());
        return element;
    }

    @Override
    public SpellAction deserialize(JsonElement jsonElement,
    		Type typeOfT,
    		JsonDeserializationContext context) throws JsonParseException {
    	Class<?> clz;
    	SpellAction action;
        JsonObject object = jsonElement.getAsJsonObject();
        if (object.has(CLASS_META_KEY)) {
            String className = object.get(CLASS_META_KEY).getAsString();
            try {
                clz = Class.forName(className);
            } catch (Exception e) {
            	Bukkit.getLogger().severe("Can't deserialize class " + className + e.getMessage());
            	clz = SpellAction.class;
            }
            
            action = context.deserialize(jsonElement, clz);
        } else {
        	action = context.deserialize(jsonElement, typeOfT);
        }
        return action;
    }
}