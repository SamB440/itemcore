package net.islandearth.itemcore.spells;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public class SpellRegistry {
	
	@Getter
	private List<CustomSpell> registry = new ArrayList<>();
	
	public CustomSpell getSpell(String name) {
		for (CustomSpell spell : registry) {
			if (spell.getName().equals(name)) {
				return spell;
			}
		}
		return null;
	}
	
	public void addSpell(CustomSpell spell) {
		registry.add(spell);
	}
}
