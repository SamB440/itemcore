package net.islandearth.itemcore.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import net.islandearth.itemcore.ItemCore;
import net.islandearth.itemcore.items.CustomItem;

public class GiveItem extends BukkitCommand {

	public GiveItem() {
		super("GiveItem");
		this.description = "Get a custom item";
		this.usageMessage = "/GiveItem";
	}

	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {
		if (sender instanceof Player) {
			if (args.length == 1) {
				if (sender.isOp() 
						|| sender.hasPermission("ItemCore.getitem")) {
					CustomItem item = ItemCore.plugin.getItemRegistry().getItem(args[0]);
					if (item != null) {
						Player player = (Player) sender;
						player.getInventory().addItem(item.toItemStack());
						player.sendMessage(ChatColor.GREEN + "Received the item!");
					} else {
						sender.sendMessage(ChatColor.RED + "Unknown item!");
					}
				} else {
					sender.sendMessage(ChatColor.RED + "You do not have permission for this command.");
				}
			}
		} else {
			sender.sendMessage(ChatColor.RED + "You can only run this command as a player!");
		}
		return true;
	}
}
