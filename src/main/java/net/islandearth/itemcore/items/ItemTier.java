package net.islandearth.itemcore.items;

public interface ItemTier {
	
	public ItemTier getTier();
	
	public TierOption[] getTierOptions();
}
