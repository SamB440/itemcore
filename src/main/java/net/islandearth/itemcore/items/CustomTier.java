package net.islandearth.itemcore.items;

import java.util.Map;

public interface CustomTier {
	
	public Map<String, String> getCustomTiers();
}
