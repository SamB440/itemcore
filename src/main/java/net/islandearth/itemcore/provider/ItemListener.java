package net.islandearth.itemcore.provider;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle.DustOptions;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.BlockIterator;

import lombok.AllArgsConstructor;
import net.islandearth.itemcore.ItemCore;
import net.islandearth.itemcore.api.ActionPlayer;
import net.islandearth.itemcore.items.CustomItem;
import net.islandearth.itemcore.items.ItemTier;
import net.islandearth.itemcore.items.WandTier;
import net.islandearth.itemcore.spells.CustomSpell;
import net.islandearth.itemcore.spells.EntityAction;
import net.islandearth.itemcore.spells.SpellAction;
import net.islandearth.itemcore.spells.SpellParticle.ParticleType;
import net.islandearth.itemcore.spells.VelocityAction;
import net.islandearth.itemcore.ui.SpellUI;
import net.islandearth.itemcore.utils.Scheduler;

@AllArgsConstructor
public class ItemListener implements Listener {
	
	private ItemCore plugin;
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent pdie) {
		List<ItemStack> items = new ArrayList<>();
		for (CustomItem item : plugin.getItemRegistry().getSoulboundItems()) {
			items.add(item.toItemStack(pdie.getItemDrop().getItemStack().getAmount()));
		}
		
		if (items.contains(pdie.getItemDrop().getItemStack())) {
			pdie.getPlayer().sendMessage(ChatColor.RED + "That item is soulbound.");
			pdie.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onUse(PlayerInteractEvent pie) {
		ItemStack item = pie.getItem();
		Player player = pie.getPlayer();
		
		if (item == null) return;
		
		if (pie.getAction() == Action.LEFT_CLICK_AIR) {
			CustomItem custom = null;
			for (CustomItem customItem : plugin.getItemRegistry().getRegistry()) {
				if (customItem.toItemStack(item.getAmount()).equals(item)) {
					custom = customItem;
					break;
				}
			}
			
			if (custom != null) {
				for (ItemTier tier : custom.getTiers()) {
					if (tier instanceof WandTier) {
						if (player.getCooldown(custom.getMaterial()) == 0) {
							CustomSpell spell = ItemCore.plugin.getCache().getSpellFor(player, custom);
							if (spell != null) {
								
								Location location2 = player.getEyeLocation();
								
								BlockIterator bi = new BlockIterator(location2, 0D, spell.getRange() + custom.getStats().getAdditionalRange());
								Location tempBta = null;
								while (bi.hasNext()) {
									tempBta = bi.next().getLocation();
									if(tempBta.getBlock().getType() != Material.AIR) break;
									
									final Location bta = tempBta;
									spell.getParticles().forEach(particle -> {
										if (particle.getType() == ParticleType.DEFAULT) {
											player.getWorld().spawnParticle(particle.getParticle(), 
													bta, 
													particle.getCount(), 
													particle.getOffSetX(), 
													particle.getOffSetY(), 
													particle.getOffSetZ(), 
													particle.getSpeed());
										} else if (particle.getType() == ParticleType.COLOUR) {
											DustOptions dustOptions = new DustOptions(Color.fromRGB(particle.getRed(), particle.getGreen(), particle.getBlue()), 1);
											player.getWorld().spawnParticle(particle.getParticle(), 
													bta, 
													particle.getCount(), 
													particle.getOffSetX(), 
													particle.getOffSetY(), 
													particle.getOffSetZ(), 
													particle.getSpeed(),
													dustOptions);
										}
									});
								}
								
								if (!spell.getActions().isEmpty()) {
									for (SpellAction action : spell.getActions()) {
										if (action.isShift() == player.isSneaking()) {
											if (action instanceof VelocityAction) {
												VelocityAction va = (VelocityAction) action;
												va.use(player);
												if (!plugin.getActions().containsKey(player.getUniqueId())) {
													plugin.getActions().put(player.getUniqueId(), new ActionPlayer(player, va.getY() * 20));
												} else {
													plugin.getActions().replace(player.getUniqueId(), new ActionPlayer(player, va.getY() * 20));
												}
											} else if (action instanceof EntityAction) {
												EntityAction ea = (EntityAction) action;
												ea.use(player, custom);
											}
											
											Scheduler scheduler = new Scheduler();
											scheduler.setTask(Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
												if (player.isOnGround()) {
													plugin.getActions().remove(player.getUniqueId());
													scheduler.cancel();
												}
											}, 0L, 20L));
										}
									}
								}
								
								player.setCooldown(custom.getMaterial(), custom.getStats().getReloadSpeed());
							} else {
								player.sendMessage(ChatColor.RED + "Select a spell first!");
							}
						}
					}
				}
			}
		} else if (pie.getAction() == Action.RIGHT_CLICK_AIR) {
			CustomItem custom = null;
			for (CustomItem customItem : plugin.getItemRegistry().getRegistry()) {
				if (customItem.toItemStack(item.getAmount()).equals(item)) {
					custom = customItem;
					break;
				}
			}
			
			if (custom != null) {
				new SpellUI(9, "Spells", custom).openInventory(player);
			}
		}
	}
}
