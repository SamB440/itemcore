package net.islandearth.itemcore.items;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.islandearth.itemcore.ItemCore;

@AllArgsConstructor
public class CustomItem {

	@Getter
	@Setter
	private boolean soulbound;
	
	@Getter
	@Setter
	private String name;
	
	@Getter
	@Setter
	private Material material;
	
	@Getter
	@Setter
	private List<String> lore;
	
	@Getter
	@Setter
	private String displayName;
	
	@Getter
	@Setter
	private List<ItemTier> tiers;
	
	@Getter
	@Setter
	private int amount;
	
	@Getter
	@Setter
	private ItemStats stats;
	
	@Getter
	@Setter
	private List<String> spells;
	
	public ItemStack toItemStack(int amount) {
		ItemStack a = new ItemStack(material, amount);
		ItemMeta am = a.getItemMeta();
		am.setDisplayName(displayName);
		am.setLore(lore);
		a.setItemMeta(am);
		return a;
	}
	
	public ItemStack toItemStack() {
		return toItemStack(amount);
	}
	
	public void saveItem() {
		Gson gson = new GsonBuilder().
				setPrettyPrinting().
				serializeNulls().
				create();
		try {
			Writer writer = new FileWriter(ItemCore.plugin.getDataFolder() + "/items/builtin/" + name + ".json");
			gson.toJson(this, writer);
			writer.close();
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}